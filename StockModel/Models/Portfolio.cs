﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace StockModel.Model
{
    public class Portfolio
    {
        public int Id { get; set; }
        public double InvestSum { get; set; }
        public List<Stock> Stocks { get; set; }
        public double ProfitOfPort { get; set; }
        public double Variance { get; set; }
        public double[,] CovMatr { get; set; }
        public double[] Parts { get; set; }
        public static string path { get; set; }


        public Portfolio()
        {
            Stocks = new List<Stock> { new Stock(), new Stock(), new Stock() };
            CovMatr = new double[3, 3];
            Reed(path);
            Calc();
        }

        private void CalculateCovar()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i == j)
                        CovMatr[i, j] = Stocks[i].Disp;
                    else
                        CovMatr[i, j] = Stocks[i].Covar(Stocks[j]);
                }
            }
        }


        private void CalculateVar()
        {
            //Parts = new double[]{0.55938361M, 0.11637032M, 0.32424707M};
            Parts = new double[] { 0.3, 0.35, 0.35 };
            var arraytemp = new double[] { 0, 0, 0 };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    arraytemp[i] += Parts[j] * CovMatr[i, j];
                }
            }

            for (int i = 0; i < 3; i++)
            {
                Variance += arraytemp[i] * Parts[i];
            }



        }
        private double CalculateVar(double _i, double _j, double _k)
        {
            var parts = new double[] { _i, _j, _k };
            double _variance = 0;
            var arraytemp = new double[] { 0, 0, 0 };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    arraytemp[i] += parts[j] * CovMatr[i, j];
                }
            }

            for (int i = 0; i < 3; i++)
            {
                _variance += arraytemp[i] * parts[i];
            }

            return _variance;

        }

        private void Optimized()
        {
            double min = 1;
            double[] _parts = { 0, 0, 0 };

            for (double i = 0; i < 1; i += 0.001)
            {
                for (double j = 0; j < 1; j += 0.001)
                {
                    for (double k = 0; k < 1; k += 0.001)
                    {
                        if ((i + j + k) >= 0.999 && (i + j + k) < 1.001)
                        {
                            var t = CalculateVar(i, j, k);
                            if (t < min)
                            {
                                min = t;
                                _parts = new double[] { i, j, k };
                            }
                        }
                    }
                }
            }

            Parts = _parts;
            Variance = min;
        }

        private void Reed(string s)
        {
            if (s==null)
            {
                s = @"data\stocks.txt";
            }

            using (var sr = new StreamReader(s))
            {
                var str = sr.ReadLine();
                while (!string.IsNullOrEmpty(str))
                {
                    var arr = str.Split('\t');
                    Stocks[0].Close.Add(Convert.ToDouble(arr[0]));
                    Stocks[1].Close.Add(Convert.ToDouble(arr[1]));
                    Stocks[2].Close.Add(Convert.ToDouble(arr[2]));
                    str = sr.ReadLine();
                }
            }
        }

        private void CalcProfit()
        {
            double profit = 0;
            for (int i = 0; i < 3; i++)
            {
                profit += Stocks[i].MO * Parts[i];
            }

            ProfitOfPort = profit;
        }

        public void Calc()
        {
            foreach (var st in Stocks)
            {
                st.Calculations();
            }
            CalculateCovar();
            Optimized();
            CalcProfit();
        }

    }
}
