﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockModel.Model
{
    public class Stock
    {
        public int Id { get; set; }
        public double MO { get; set; }
        public double Disp { get; set; }
        public List<double> Close { get; set; }
        public List<double> Profit { get; set; }
        public int? PortId { get; set; }


        public Stock()
        {
            Close = new List<double>();
            Profit = new List<double>();
        }

        private void CalculateProfit()
        {
            for (int i = 0; i < Close.Count - 1; i++)
            {
                if (i != Close.Count - 1)
                {
                    Profit.Add((Close[i + 1] - Close[i]) / Close[i]);
                }
            }
        }

        private void Aver()
        {
            MO = Profit.Average();
        }

        private void Var()
        {
            double sum = 0;
            for (int i = 0; i < Profit.Count; i++)
            {
                sum += (Profit[i] - MO) * (Profit[i] - MO);
            }

            Disp = sum / Profit.Count;
        }

        public double Covar(Stock stock)
        {
            double sum = 0;
            for (int i = 0; i < Profit.Count; i++)
            {
                sum += (Profit[i] - MO) * (stock.Profit[i] - stock.MO);
            }

            return sum / Profit.Count;
        }

        public void Calculations()
        {
            CalculateProfit();
            Aver();
            Var();

        }
    }
}
