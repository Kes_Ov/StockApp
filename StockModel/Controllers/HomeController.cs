﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using StockModel;
using Microsoft.AspNetCore.Mvc;
using StockModel.Model;



namespace StockModel.Controllers
{
    public class HomeController : Controller
    {
        private string path;
        private Portfolio port = new Portfolio();


        public IActionResult MainPage()
        {
            return View();
        }
        public IActionResult Index()
        {
            
            return View(port);
            
        }
       
        public IActionResult Profit()
        {
            return View(port.Stocks);
        }

        public IActionResult Stats()
        {

            return View(port);
        }

        [HttpPost]
        public IActionResult AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                path =  uploadedFile.FileName;
                Portfolio.path = path;
            }

            return RedirectToAction("Index");
        }



    }
}
